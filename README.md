# README #

FortiOS one-arm policies finder

## Files ##

### onearmpoliciesfinder.py ###

This will find one-arm policies within a config file (policies with same source and destination interface)

Run it from within the directory containing firewall config(s), then follow instructions on screen.

Without arguments it will display one-arm policy ID's in their respective VDOM's

### README.md ###

This file

## Usage ##

onearmpoliciesfinder.py [-h] [-c]

optional arguments:

  -h, --help    show this help message and exit

  -c, --config  Print ready to copy/paste config to delete one-arm policies